import time

import redis
from flask import Flask, request

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)


@app.route('/', methods=['POST'])
def record():
	data = request.get_data()
	cache.set(time.time(), str(data))
	return data

@app.route('/', methods=['GET'])
def getdata():
	return(str(cache.keys()))

if __name__ == "__main__":
    app.run(host= '0.0.0.0')

# hola